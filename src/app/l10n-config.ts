import {L10nConfig, L10nProvider, L10nTranslationLoader} from 'angular-l10n';
import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';

export const l10nConfig: L10nConfig = {
  format: 'language-region',
  providers: [
    {name: 'app', asset: 'app'}
  ],
  cache: true,
  keySeparator: '.',
  defaultLocale: {language: 'de-DE', currency: 'EUR', timeZone: 'Europe/Berlin'},
  schema: [
    {locale: {language: 'de-DE', currency: 'EUR', timeZone: 'Europe/Berlin'}},
    {locale: {language: 'en-US', currency: 'USD', timeZone: 'America/Los_Angeles'}},
  ]
};

@Injectable()
export class TranslationLoader implements L10nTranslationLoader {
  public get(language: string, provider: L10nProvider): Observable<{ [key: string]: any }> {
    const data = import(`../i18n/${language}/${provider.asset}.json`);
    return from(data);
  }
}
