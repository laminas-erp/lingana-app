export class BaseApiResponseGeneric<T> {
  constructor(
    public uuid: string,
    public desc: string,
    public success: boolean,
    public messages: [],
    public obj: T, // single value
    public arr: T[], // multiple values
    public count: number
  ) {
  }
}
