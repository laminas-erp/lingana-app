import {BaseClass} from "./base-class.model";

export class RightsnrolesUser extends BaseClass {
  public user_role_id = -1;
  public user_role_alias = '';

  /**
   * object.key = user_group_alias
   * object.value: number = user_group_name
   */
  public user_groups: any = {};

  /**
   * object.key = user_right_alias
   * object.value: number = user_right_relation_value
   */
  public user_rights: any = {};

  /**
   * object.key = user_role_special_alias
   * object.value = user_role_special_value
   */
  public user_role_specials: any = {};
}
