import {BaseClass} from './base-class.model';

export class UserMin extends BaseClass {
  user_uuid = '';
  user_login = '';
  user_active = 0;
  user_email = '';
  user_lang_iso = '';
}
