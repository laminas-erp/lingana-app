import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, Subscription} from 'rxjs';
import {filter, tap} from 'rxjs/operators';
import {UserAuthService} from '../service/user-auth.service';
import {MessageService} from '../service/message.service';
import {Injectable, OnDestroy} from '@angular/core';
import {CommonAppService} from '../service/common-app.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class BaseHttpInterceptor implements HttpInterceptor, OnDestroy {
  subscriptions = new Set<Subscription>();
  sessionKey = environment.session_key;
  apiBaseUrl = '';

  constructor(
    private cs: CommonAppService,
    public userAuth: UserAuthService,
    public messageService: MessageService,
  ) {
    this.apiBaseUrl = cs.getApiBaseUrl();
    this.subscriptions.add(this.cs.changedApiBaseUrl.subscribe((apiBaseUrl: string) => {
      this.apiBaseUrl = apiBaseUrl;
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.substring(0, 7) !== '/assets') {
      if (!req.url.includes('autocomplete')) {
      }
      req = req.clone({
        url: this.apiBaseUrl + req.url,
        withCredentials: true,
        setHeaders: {
          SESSIONHASH: window.localStorage.getItem(this.sessionKey) + ''
        }
      });
    }

    return next.handle(req).pipe(
      tap({
        next: (next) => {
        },
        error: (error) => {
          console.log('error.status: ' + error.status);
          if (error.status === 403) {
            // this.messageService.showMessage('warn', '', 'Es fehlen die erforderlichen Rechte.');
            if (this.userAuth.getSessionHash().length > 0) {
              this.messageService.showMessage('warn', '', 'Es fehlen die erforderlichen Rechte.');
            } else {
              this.userAuth.handleLoggedOut();
              this.userAuth.navigateLogin();
            }
          } else if (error.status === 404) {
            this.messageService.showMessage('warn', '', 'Die Daten der Anfrage waren nicht OK.');
          } else if (error.status === 400) {
            const messs: string[] = [];
            if (error.error && error.error.messages && error.error.messages.length > 0) {
              error.error.messages.forEach((mess: string) => {
                messs.push(mess);
              });
            } else {
              messs.push('Der Server gab den Fehler 400 zurück!');
            }
            this.messageService.showMessages('warn', '', messs);
          } else if (error.status === 503) {
            // purge all of the session data
            this.userAuth.handleLoggedOut();
            this.userAuth.navigateLogin();
          }
        }
      })
    );
  }

}
