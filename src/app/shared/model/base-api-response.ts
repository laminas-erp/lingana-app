import {KeyValObj} from './key-val-obj.model';

export class BaseApiResponse {

  constructor(
    public uuid: string,
    public desc: string,
    public success: boolean,
    public messages: string[] = [],
    public formMessages: string[] = [],
    public val: string,
    public obj: {}, // single value
    public arr: [], // multiple values
    public keyValObjArr: [KeyValObj],
    public count: number
  ) {
  }

}
