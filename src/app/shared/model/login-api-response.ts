import {RightsnrolesUser} from './rightsnroles-user';
import {BaseApiResponse} from './base-api-response';
import {UserMin} from "./user-min.model";

export class LoginApiResponse extends BaseApiResponse {
  session_hash!: string;
  rightsnroles!: RightsnrolesUser;
  user_uuid!: string;
  user!: UserMin;
}
