import {BaseClass} from "./base-class.model";

export class Contact extends BaseClass {
  contact_uuid = '';
  contact_label = '';
  contact_salut = '';
  contact_name = '';
  contact_dept = '';
  contact_tel = '';
  contact_fax = '';
  contact_mobile = '';
  contact_email = '';
  contact_www = '';
  contact_desc = '';
}
