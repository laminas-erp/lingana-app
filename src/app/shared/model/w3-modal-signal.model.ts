/**
 * Good implemented in UsersComponent
 */
export class W3ModalSignal {
  /**
   * if null then all modal are closed/opened
   */
  id: string | null;
  styleDisplay: boolean;

  constructor(id: string, styleDisplay: boolean) {
    this.id = id;
    this.styleDisplay = styleDisplay;
  }

  setId(id: string | null): W3ModalSignal {
    this.id = id;
    return this;
  }

  setStyleDisplay(styleDisplay: boolean): W3ModalSignal {
    this.styleDisplay = styleDisplay;
    return this;
  }
}
