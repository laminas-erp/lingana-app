import {BaseClass} from "./base-class.model";

export class ProjectType extends BaseClass {
  project_type_id = 0;
  project_type_label = '';
  project_type_order_priority = 0;
}
