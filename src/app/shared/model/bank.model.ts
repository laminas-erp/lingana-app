import {BaseClass} from "./base-class.model";

export class Bank extends BaseClass {
  bank_uuid = '';
  bank_label = '';
  bank_institute = '';
  bank_holder = '';
  bank_iban = '';
  bank_bic = '';
  bank_code_bank = '';
  bank_code_holder = '';
  bank_time_create = '';
}
