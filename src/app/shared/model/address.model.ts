import {BaseClass} from "./base-class.model";

export class Address extends BaseClass {
  address_uuid = '';
  address_label = '';
  address_name = '';
  address_name2 = '';
  address_street = '';
  address_street_no = '';
  address_zip = '';
  address_city = '';
  country_id = 0;
  country_name = '';
  country_iso = '';
  country_member_eu = 0;
  country_currency_euro = 0;
}
