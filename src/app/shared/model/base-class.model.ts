export class BaseClass {
  asFormData: string[] = [];
  /**
   * field name list
   */
  htmlentitiesParts: string[] = [];

  formExcludes: string[] = ['formExcludes', 'asFormData', 'htmlentitiesParts'];

  exchangeObject(obj: object) {
    const keys = Object.keys(obj);
    for (const key of keys) {
      if (!this.hasOwnProperty(key)) {
        continue;
      }
      // @ts-ignore
      this[key] = obj[key];
    }
  }

  getPlainobject() {
    const keys = Object.keys(this);
    const obj = {};
    for (const key of keys) {
      // @ts-ignore
      obj[key] = this[key];
    }
    return obj;
  }

  getPlainobjectPureged() {
    const keys = Object.keys(this);
    const obj = {};
    for (const key of keys) {
      if (!this.hasOwnProperty(key) || this.formExcludes.includes(key) || (this.asFormData.length > 0 && !this.asFormData.includes(key))) {
        continue;
      }
      // @ts-ignore
      obj[key] = this[key];
    }
    return obj;
  }

  getAsFormData() {
    const keys = Object.keys(this);
    const form = new FormData();
    for (const key of keys) {
      if (!this.hasOwnProperty(key) || this.formExcludes.includes(key) || (this.asFormData.length > 0 && !this.asFormData.includes(key))) {
        continue;
      }
      // @ts-ignore
      form.append(key, this[key]);
    }
    return form;
  }

  htmlentitiesDecode() {
    const dp = new DOMParser();
    const keys = Object.keys(this);
    for (const key of keys) {
      // @ts-ignore
      if (!this.hasOwnProperty(key) || this[key] === null) {
        continue;
      }
      // @ts-ignore
      this[key] = dp.parseFromString(this[key], 'text/html').documentElement.textContent;
    }
  }

  htmlentitiesDecodePart() {
    const dp = new DOMParser();
    const keys = Object.keys(this);
    for (const key of keys) {
      // @ts-ignore
      if (!this.hasOwnProperty(key) || this[key] === null || (this.htmlentitiesParts.length > 0 && !this.htmlentitiesParts.includes(key))) {
        continue;
      }
      // @ts-ignore
      this[key] = dp.parseFromString(this[key], 'text/html').documentElement.textContent;
    }
  }

  // @ts-ignore
  getValueP(field: string) {
    if (this.hasOwnProperty(field)) {
      const dp = new DOMParser();
      // @ts-ignore
      return dp.parseFromString(this[field], 'text/html').documentElement.textContent;
    }
  }
}
