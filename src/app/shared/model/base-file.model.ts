import {BaseClass} from "./base-class.model";

export class BaseFile extends BaseClass {
  file_uuid = '';
  file_label = '';
  file_desc = '';
  file_filename = '';
  file_extension = '';
  file_mimetype = '';
  file_time_create = '';
  file_time_update = '';
  file_category_id = 0;

  file_input: any;
}
