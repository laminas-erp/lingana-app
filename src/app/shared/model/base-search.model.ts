import {BaseClass} from './base-class.model';

export class BaseSearch extends BaseClass {
  order_field = '';
  order_direc = 'DESC';
  offset = 0; // computed from setOffset()
  limit = 10; // items on page
  count = 0; // all - from server

  initOrder() {
    let arrow = '';
    if (this.order_direc === 'DESC') {
      arrow = '<i class="fas fa-long-arrow-alt-down"></i>';
    } else {
      arrow = '<i class="fas fa-long-arrow-alt-up"></i>';
    }
    $('.order-direc-arrow').html('');
    $('#arrow_' + this.order_field).html(arrow);
  }

  computeOrder(orderField: string) {
    let arrow = '';
    if (this.order_field !== orderField) {
      this.order_field = orderField;
      this.order_direc = 'DESC';
      arrow = '<i class="fas fa-long-arrow-alt-down"></i>';
    } else {
      if (this.order_direc === 'DESC') {
        this.order_direc = 'ASC';
        arrow = '<i class="fas fa-long-arrow-alt-up"></i>';
      } else {
        this.order_direc = 'DESC';
        arrow = '<i class="fas fa-long-arrow-alt-down"></i>';
      }
    }
    $('.order-direc-arrow').html('');
    $('#arrow_' + orderField).html(arrow);
  }

  computeQueryParams(withQuestionMark = true): string {
    const searchKeys = Object.keys(this);
    let queryParams = withQuestionMark ? '?' : '';
    let count = 0;
    for (const key of searchKeys) {
      if (this.hasOwnProperty(key)
        && typeof (this as any)[key] !== 'function'
        && (typeof (this as any)[key] !== 'object'
          || Array.isArray((this as any)[key]))) {
        if (!withQuestionMark || count > 0) {
          queryParams += '&';
        }
        if (typeof (this as any)[key] === 'undefined') {
          (this as any)[key] = '';
        }
        if (Array.isArray((this as any)[key])) {
          queryParams += key + '=' + (this as any)[key].join(',');
        } else {
          queryParams += key + '=' + (this as any)[key];
        }
        count++;
      }
    }
    return queryParams;
  }

  computeFormData(): FormData {
    const searchKeys = Object.keys(this);
    let form = new FormData();
    for (const key of searchKeys) {
      if (this.hasOwnProperty(key) && typeof (this as any)[key] !== 'object' && typeof (this as any)[key] !== 'undefined') {
        form.append(key, (this as any)[key]);
      }
    }
    return form;
  }
}
