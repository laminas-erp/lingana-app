import {Component} from '@angular/core';
import {Message, MessageService} from '../service/message.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-message-alert',
  templateUrl: './message-alert.component.html',
  styleUrls: ['./message-alert.component.scss']
})
export class MessageAlertComponent {

  messageTimeout = 5000;
  message: Message = new Message('info', '', '');
  styleDisplay = 'none';

  constructor(public messageService: MessageService) {
    this.messageService.appMessage.subscribe((message: Message) => {
      this.message = message;
      this.show();
    });
    this.messageService.appFormMessages.subscribe((messages: string[]) => {
      this.glueMessagesOnElements(messages);
    });
    this.messageService.appFormClear.subscribe((clear: boolean) => {
      if (clear) {
        this.clearMessagesOnElements();
      }
    });
  }

  show() {
    this.styleDisplay = 'block';
    if (this.message.withTimeout) {
      window.setTimeout(() => this.hide(), this.messageTimeout);
    }
  }

  hide() {
    this.styleDisplay = 'none';
  }

  glueMessagesOnElements(messages: string[]) {
    this.clearMessagesOnElements();
    let messagesForAlert = '';
    for (const message of messages) {
      const splittet = message.split('|#|');
      if (splittet.length !== 2) {
        messagesForAlert += '\n' + splittet[0];
        continue;
      }
      const elem = $('#' + splittet[0]);
      if (elem) {
        const tempElem = document.createElement('div');
        tempElem.style.color = 'red';
        tempElem.innerText = splittet[1];
        tempElem.classList.add('elemessage');
        $(elem).after(tempElem.outerHTML);
        $(elem).css('background-color', 'red');
        // for nested input-elements - e.g. datepicker
        $('#' + splittet[0] + ' input').css('background-color', 'red');
      }
    }
    if (messagesForAlert) {
      this.message = new Message('warn', '', messagesForAlert);
      this.show();
    }
  }

  clearMessagesOnElements() {
    $('.elemessage').remove();
    $('input').css('background-color', '');
    $('select').css('background-color', '');
    $('textarea').css('background-color', '');
  }

}
