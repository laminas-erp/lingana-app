import {Component, Input, OnInit} from '@angular/core';
import {svg_icon_path} from "../../app-prefs";

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent implements OnInit {
  @Input() filename = '';
  @Input() alt = 'icon';
  @Input() widthPx = 0;
  @Input() heightPx = 0;
  @Input() preserveAspectRatio = true;
  src = '';
  width = '';
  height = '';

  constructor() {
  }

  ngOnInit(): void {
    this.src = svg_icon_path + this.filename + '.svg';
    this.width = this.widthPx ? this.widthPx + 'px' : '';
    this.height = this.heightPx ? this.heightPx + 'px' : '';
  }

}
