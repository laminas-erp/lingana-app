import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SharedRoutingModule} from './shared-routing.module';
import {PageTopBarComponent} from "./page-top-bar/page-top-bar.component";
import {PaginationComponent} from "./pagination/pagination.component";
import {SvgIconComponent} from "./svg-icon/svg-icon.component";
import {MessageAlertComponent} from "./message-alert/message-alert.component";
import {LoginComponent} from "./login/login.component";
import {ParseHtmlPipe} from "./pipe/parse-html.pipe";
import {HideNullPipe} from "./pipe/hide-null.pipe";
import {HideZeroPipe} from "./pipe/hide-zero.pipe";
import {MapEntriesPipe} from "./pipe/map-entries.pipe";
import {ShorterTextPipe} from "./pipe/shorter-text.pipe";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    PageTopBarComponent, PaginationComponent,
    SvgIconComponent, MessageAlertComponent,
    LoginComponent,
    HideNullPipe, HideZeroPipe, MapEntriesPipe, ParseHtmlPipe, ShorterTextPipe
  ],
  imports: [
    CommonModule, FontAwesomeModule,
    SharedRoutingModule, FormsModule
  ],
  exports: [
    PageTopBarComponent, PaginationComponent,
    SvgIconComponent, MessageAlertComponent,
    LoginComponent,
    HideNullPipe, HideZeroPipe, MapEntriesPipe, ParseHtmlPipe, ShorterTextPipe
  ],
})
export class SharedModule {
}
