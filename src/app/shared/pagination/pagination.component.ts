import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {faBackward, faForward} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnChanges {

  protected readonly faForward = faForward;
  protected readonly faBackward = faBackward;

  @Input() count = 0;
  @Input() pageSize = 0;
  @Input() pageCurrent = 0;
  @Output() pageCurrentOut = new EventEmitter<number>();
  @Output() pageSizeOut = new EventEmitter<number>();
  pageCount = 0;
  pageNumbers: number[] = [];

  constructor() {
  }

  ngOnChanges() {
    this.pageCount = Math.ceil(this.count / this.pageSize);
    this.pageNumbersChange();
  }

  pageNumbersChange() {
    this.pageNumbers = [];
    for (let i = 0; i < this.pageCount; i++) {
      if (i > 20) {
        break;
      }
      this.pageNumbers.push(i);
    }
  }

  pageSizeChanged(pageSize: number) {
    this.pageSizeOut.emit(pageSize);
  }

  goToPage(page: number) {
    this.pageCurrent = page;
    this.pageCurrentOut.emit(page);
  }
}
