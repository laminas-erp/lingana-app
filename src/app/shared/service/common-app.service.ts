import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, map, Observable, Subject, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {l10nConfig} from '../../l10n-config';
import * as moment from 'moment';
import {BaseApiResponse} from '../model/base-api-response';

@Injectable({
  providedIn: 'root'
})
export class CommonAppService {

  private changedApiBaseUrlSource = new Subject<string>(); // ...to fire next()
  public changedApiBaseUrl = this.changedApiBaseUrlSource.asObservable(); // ...to listen for foo.next()

  constructor(
    private http: HttpClient,
  ) {
  }

  getApiBaseUrl(): string {
    return window.localStorage.getItem('apiBaseUrl') ?? '';
  }

  saveApiBaseUrl(apiBaseUrl: string) {
    window.localStorage.setItem('apiBaseUrl', apiBaseUrl);
    this.changedApiBaseUrlSource.next(apiBaseUrl);
  }

  resetApiBaseUrl() {
    this.saveApiBaseUrl(environment.apiUrl);
  }

  isNativePlatform(): boolean {
    return window.localStorage.getItem('isNative') === '1';
  }

  apiPing(): Observable<boolean> {
    return this.http.get<BaseApiResponse>('/ping').pipe(map(response => response.success), catchError(this.handleHttpErrorResponse));
  }

  /**
   * Display errors in console.
   *
   * @param err Die HttpErrorResponse vom Server
   */
  handleHttpErrorResponse(err: HttpErrorResponse): Observable<any> {
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', err.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(JSON.stringify(err, null, 4));
      console.error('Backend returned code: ' + err.status);
      console.error('body was: ' + err.error);
    }
    // return an observable with a user-facing error message
    return throwError(() => 'Something bad happened; please try again later.');
  }

  /**
   * For objects that do not have the function computeQueryParams() from the base class BaseSearch().
   *
   * @param searchObject
   * @param fields Available fields
   */
  computeQueryParamsFromSearchObject(searchObject: object, fields: string[] = []): string {
    const searchKeys = Object.keys(searchObject);
    let queryParams = '?';
    let count = 0;
    for (const key of searchKeys) {
      if (fields.length && !fields.includes(key)) {
        continue;
      }
      // @ts-ignore
      if (searchObject.hasOwnProperty(key) && typeof searchObject[key] !== 'function'
        // @ts-ignore
        && (typeof searchObject[key] !== 'object' || Array.isArray(searchObject[key]))) {
        if (count > 0) {
          queryParams += '&';
        }
        // @ts-ignore
        if (typeof searchObject[key] === 'undefined') {
          // @ts-ignore
          searchObject[key] = '';
        }
        // @ts-ignore
        if (Array.isArray(searchObject[key])) {
          // @ts-ignore
          queryParams += key + '=' + encodeURIComponent(searchObject[key].join(','));
        } else {
          // @ts-ignore
          queryParams += key + '=' + encodeURIComponent(searchObject[key]);
        }
      }
      count++;
    }
    return queryParams;
  }

  computeStringStringMap(obj: object): Map<string, string> {
    const assoc = new Map<string, string>();
    const keys = Object.keys(obj);
    for (const key of keys) {
      if (obj.hasOwnProperty(key)) {
        // @ts-ignore
        assoc.set(key, obj[key]);
      }
    }
    return assoc;
  }

  htmlEntitiesReal(str: string): string {
    return String(str)
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;');
  }

  /**
   * The current DateTime with default timeZone from 'l10nConfig'.
   */
  getCurrentDateTime(): Date {
    return new Date(new Date().toLocaleString('en-US', {timeZone: l10nConfig.defaultLocale.timeZone}));
  }

  getCurrentDateTimeISOString(): string {
    return moment(this.getCurrentDateTime()).toISOString(true).substring(0, 16);
  }

  toIonicDatetime(datetime: string | undefined) {
    if (!datetime) {
      return this.getCurrentDateTimeISOString().replace(' ', 'T').substring(0, 16);
    }
    return moment(new Date(datetime)).toISOString(true).replace(' ', 'T').substring(0, 16);
  }

  toSqlDatetime(datetime?: string) {
    if (!datetime) {
      return this.getCurrentDateTimeISOString().replace('T', ' ').substring(0, 16);
    }
    return moment(new Date(datetime)).toISOString(true).replace('T', ' ').substring(0, 16);
  }
}
