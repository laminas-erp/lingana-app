import {Injectable, OnDestroy} from '@angular/core';
import {catchError, map, Observable, Subscription} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Device} from "@capacitor/device";
import {RightsnrolesUser} from "../model/rightsnroles-user";
import {LoginApiResponse} from "../model/login-api-response";
import {Router} from "@angular/router";
import {BaseApiResponse} from "../model/base-api-response";

const LOCALSTORAGE_KEY_RIGHTSNROLES = 'rightsnroles';
/**
 * 1 = not logged in
 * 2 = not logged in
 * 3 = logged in
 */
const LOCALSTORAGE_KEY_LOGGEDIN_STATUS = 'loggedinstatus';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService implements OnDestroy {
  subscriptions = new Set<Subscription>();
  sessionKey = environment.session_key;
  private deviceId = '';
  private rightsnroles: RightsnrolesUser = new RightsnrolesUser();

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    Device.getId().then(id => {
      this.deviceId = id.identifier;
    });
    const rr = window.localStorage.getItem(LOCALSTORAGE_KEY_RIGHTSNROLES);
    if (rr && rr.length > 0) {
      try {
        this.rightsnroles.exchangeObject(JSON.parse(rr));
      } catch (e) {
        console.log('undefined JSON in user-auth.service.ts');
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  getLoggedInStatus(): string {
    return window.localStorage.getItem(LOCALSTORAGE_KEY_LOGGEDIN_STATUS) ?? '0';
  }

  getSessionHash(): string {
    return window.localStorage.getItem(this.sessionKey) ?? '';
  }

  /**
   * 1 = 2FA Authenticator Secret was send
   * 2 = 2FA Code is required
   * 3 = logged in
   * @param status the status for comparison
   */
  isLoggedInStatus(status:string = '3'): boolean {
    return this.getLoggedInStatus() === status;
  }

  isLoggedIn(): boolean {
    return this.isLoggedInStatus();
  }

  handleLoggedOut() {
    window.localStorage.removeItem(this.sessionKey);
    window.localStorage.removeItem(LOCALSTORAGE_KEY_LOGGEDIN_STATUS);
    window.localStorage.removeItem(LOCALSTORAGE_KEY_RIGHTSNROLES);
    this.rightsnroles = new RightsnrolesUser();
  }

  handleLoginApiResponse(loginApiResponse: LoginApiResponse) {
    if (loginApiResponse.success && loginApiResponse.session_hash.length > 0) {
      window.localStorage.setItem(this.sessionKey, loginApiResponse.session_hash);
      window.localStorage.setItem(LOCALSTORAGE_KEY_LOGGEDIN_STATUS, '3');
      window.localStorage.setItem(LOCALSTORAGE_KEY_RIGHTSNROLES, JSON.stringify(loginApiResponse.rightsnroles));
      this.rightsnroles = new RightsnrolesUser();
      this.rightsnroles.exchangeObject(loginApiResponse.rightsnroles);
    } else {
      this.handleLoggedOut();
    }
  }

  /**
   * Fetch actual data for current logged-in user.
   */
  sessionCheck() {
    this.subscriptions.add(this.http.get<LoginApiResponse>('/session-check').subscribe((data) => {
      this.handleLoginApiResponse(data);
    }));
  }

  login(login: string, passwd: string): void {
    const formData = new FormData();
    formData.append('login', login);
    formData.append('passwd', passwd);
    this.subscriptions.add(this.http.post<LoginApiResponse>('/login', formData).subscribe((data) => {
      this.handleLoginApiResponse(data);
      if (this.isLoggedIn()) {
        window.localStorage.setItem('useruuid', data.user_uuid);
        window.localStorage.setItem('username', data.user.user_login);
        window.localStorage.setItem('useremail', data.user.user_email);
        this.router.navigate(['/home']);
      }
    }, (error: HttpErrorResponse) => {
      if (error.status === 403) {
        this.handleLoggedOut();
      }
    }));
  }

  logout(): void {
    this.subscriptions.add(this.http.get<BaseApiResponse>('/logout').subscribe((data) => {
      this.logoutApplication();
    }));
  }

  logoutApplication(): void {
    this.handleLoggedOut();
    this.navigateLogin();
  }

  isUserInRole(roleId: number): boolean {
    if (this.rightsnroles === null) {
      return false;
    }
    return this.rightsnroles.user_role_id === roleId;
  }

  isUserInRoleMin(roleId: number): boolean {
    return this.rightsnroles.user_role_id <= roleId;
  }

  hasUserRight(userRightAlias: string, userRightRelationValue = 1): boolean {
    if (!this.rightsnroles.user_rights.hasOwnProperty(userRightAlias)) {
      return false;
    }
    return this.rightsnroles.user_rights[userRightAlias] >= userRightRelationValue;
  }

  isUserInGroup(userGroupAlias: string): boolean {
    if (!this.rightsnroles.user_groups.hasOwnProperty(userGroupAlias)) {
      return false;
    }
    return this.rightsnroles.user_groups[userGroupAlias].length > 0;
  }

  navigateLogin() {
    this.router.navigate(['/login']).then(r => window.location.reload());
  }

}
