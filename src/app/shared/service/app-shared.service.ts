import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {PayTerm} from '../model/pay-term.model';
import {Skr03} from '../model/skr03.model';
import {W3ModalSignal} from '../model/w3-modal-signal.model';
import {ProjectType} from '../model/project-type.model';
import {MessageService} from "./message.service";
import {BaseApiResponse} from "../model/base-api-response";
import {CommonAppService} from "./common-app.service";
import {BaseApiResponseGeneric} from "../model/base-api-response-generic.model";

@Injectable({
  providedIn: 'root'
})
export class AppSharedService {

  private w3ModalShowSource = new Subject<W3ModalSignal>(); // ...to fire next()
  public w3ModalShow = this.w3ModalShowSource.asObservable(); // ...to listen for

  private fileFormResetSource = new Subject<boolean>(); // ...to fire next()
  public fileFormReset = this.fileFormResetSource.asObservable(); // ...to listen for

  constructor(public http: HttpClient, private messageService: MessageService, private commonAppService: CommonAppService) {
  }

  modalShowNext(w3ModalShow: W3ModalSignal) {
    this.w3ModalShowSource.next(w3ModalShow);
  }

  fileFormResetNext(doReset: boolean) {
    this.fileFormResetSource.next(doReset);
  }

  getIndustryCategoryUuidAssoc(): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-common-lists-industry-category').pipe(map(
      response => {
        const categories = new Map<string, string>();
        const uuids = Object.getOwnPropertyNames(response.obj);
        for (const uuid of uuids) {
          if (response.obj.hasOwnProperty(uuid)) {
            categories.set(uuid, (response.obj as any)[uuid]);
          }
        }
        return categories;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getCountryIdAssoc(): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-common-rest-countrylist').pipe(map(
      response => {
        const stringMap = new Map<string, string>();
        const ids = Object.getOwnPropertyNames(response.obj);
        for (const id of ids) {
          if (response.obj.hasOwnProperty(id)) {
            stringMap.set(id, (response.obj as any)[id]);
          }
        }
        return stringMap;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getCostCentreIdAssoc(): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-common-lists-costcentre').pipe(map(
      response => {
        const stringMap = new Map<string, string>();
        const ids = Object.keys(response.obj);
        for (const id of ids) {
          if (response.obj.hasOwnProperty(id)) {
            stringMap.set(id, (response.obj as any)[id]);
          }
        }
        return stringMap;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getProjectTypes(): Observable<ProjectType[]> {
    return this.http.get<BaseApiResponse>('/lerp-common-lists-project-types').pipe(map(
      response => response.arr as ProjectType[]
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getPayTerms(): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponseGeneric<PayTerm>>('/lerp-common-lists-payterm').pipe(map(
      response => {
        const stringMap = new Map<string, string>();
        for (const payTerm of response.arr) {
          stringMap.set(payTerm.pay_term_code, payTerm.pay_term_text);
        }
        return stringMap;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getSkr03s(): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponseGeneric<Skr03>>('/lerp-common-lists-skr03?without_deposit=true').pipe(map(
      response => {
        const stringMap = new Map<string, string>();
        for (const skr03 of response.arr) {
          stringMap.set(skr03.skr_03_code, skr03.skr_03_code + ' - ' + skr03.skr_03_desc);
        }
        return stringMap;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getLangIsos(): Observable<string[]> {
    return this.http.get<BaseApiResponse>('/lerp-common-lists-lang-isos').pipe(map(
      response => response.arr
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getFileCategoriesAssoc(brand: string): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/bitkorn-files-categories-brand-assoc/' + brand).pipe(map(
      response => {
        const stringMap = new Map<string, string>();
        for (const [key, value] of Object.entries<string>(response.obj)) {
          stringMap.set(key, value);
        }
        return stringMap;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }
}
