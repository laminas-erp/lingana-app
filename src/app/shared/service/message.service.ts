import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {FormMessages} from "../model/form-messages.model";

export type MessageType = 'error' | 'warn' | 'success' | 'info';

export class Message {
  styleColor = '';
  styleBgColor = '';
  withTimeout = true;

  constructor(public messageType: MessageType, public label: string, public text: string, withTimeout = true) {
    switch (messageType) {
      case 'error':
        this.styleBgColor = 'red';
        this.styleColor = 'black';
        break;
      case 'warn':
        this.styleBgColor = '#ff9900';
        this.styleColor = 'black';
        break;
      case 'success':
        this.styleBgColor = '#009933';
        this.styleColor = 'white';
        break;
      case 'info':
        this.styleBgColor = '#0099ff';
        this.styleColor = 'white';
        break;
    }
    this.withTimeout = withTimeout;
  }
}

export const messageSeparator = '|#|';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private appMessageSource = new Subject<Message>(); // ...to fire next()
  public appMessage = this.appMessageSource.asObservable(); // ...to listen for loadProductLocationsSource.next()
  private appFormMessagesSource = new Subject<string[]>(); // ...to fire next()
  public appFormMessages = this.appFormMessagesSource.asObservable(); // ...to listen for *.next()
  private appFormClearSource = new Subject<boolean>(); // ...to fire next()
  public appFormClear = this.appFormClearSource.asObservable(); // ...to listen for *.next()

  constructor() {
  }


  async showMessage(messageType: MessageType, label: string, text: string, withTimeout: boolean = true) {
    this.appMessageSource.next(new Message(messageType, label, text, withTimeout));
  }

  showMessages(messageType: MessageType, label: string, texts: string[], withTimeout: boolean = true) {
    let text = texts.shift() as string;
    for (const t of texts) {
      text += '<br>' + t;
    }
    this.showMessage(messageType, label, text, withTimeout).then(r => {
    });
  }

  glueMessagesOnElements(messages: string[]) {
    this.appFormMessagesSource.next(messages);
  }

  clearMessagesOnElements() {
    this.appFormClearSource.next(true);
  }
}
