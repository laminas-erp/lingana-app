import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

const KEY_RIGHTSNROLES = 'rightsnroles';
const KEY_LOGGED_IN = 'logged_in';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HttpClient) { }
}
