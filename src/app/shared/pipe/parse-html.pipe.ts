import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'parseHtml'
})
export class ParseHtmlPipe implements PipeTransform {

  transform(html: string): string {
    if (!html) {
      return '';
    }
    const dp = new DOMParser();
    const doc = dp.parseFromString(html, 'text/html');
    if (doc) return <string>doc.documentElement.textContent;
    return '';
  }

}
