import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'mapEntries'
})
export class MapEntriesPipe implements PipeTransform {

  transform<K, V>(input: Map<K, V>): Array<any> {
    // Note that I used Array<any> instead of Array<{ key: K; value: V; }>
    // because the Angular Language Service extension shows compilation error
    // "Expected operands to be of comparable types or any"
    // when comparing item.key === otherValueOfKType in the template.
    // https://github.com/angular/angular/issues/31420#issuecomment-671138557
    return Array.from(input).map(([key, value]) => ({key, value}));
  }

}
