import {Pipe, PipeTransform} from '@angular/core';
import {AppSharedService} from "../service/app-shared.service";
import {CommonAppService} from "../service/common-app.service";

@Pipe({
  name: 'shorterText'
})
export class ShorterTextPipe implements PipeTransform {

  constructor(private commonAppService: CommonAppService) {
  }

  transform(longText: string, length: number, withTitle: boolean = false): string {
    if (!longText) {
      return '';
    }
    if (length === undefined || length === 0) {
      return longText;
    }
    const shortText = longText.substring(0, length++);
    let title = '';
    if (withTitle && shortText.length < longText.length) {
      title = ' <span class="w3-badge w3-blue cursor-help" title="' + this.commonAppService.htmlEntitiesReal(longText) + '">?</span>';
    }
    return shortText + title;
  }

}
