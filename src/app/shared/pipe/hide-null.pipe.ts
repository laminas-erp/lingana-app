import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'hideNull'
})
export class HideNullPipe implements PipeTransform {

  transform(value: unknown): string {
    if (!value || value === 'null') {
      return '';
    }
    return value + '';
  }

}
