import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {MessageService} from "../service/message.service";
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router";
import {UserAuthService} from "../service/user-auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login = '';
  passwd = '';
  message = '';

  apiUrl = environment.apiUrl;
  featureListDate = '';

  constructor(
    private router: Router,
    private userAuth: UserAuthService,
    private messageService: MessageService,
    private titleService: Title,
  ) {
    if (userAuth.isLoggedIn()) {
      this.router.navigate(['home']);
    }
  }

  ngOnInit() {
    this.titleService.setTitle('Lingana Login');
  }


  sendLogin(): void {
    this.userAuth.login(this.login, this.passwd);
  }
}
