import {Component, Input, Output, EventEmitter} from '@angular/core';
import { faTimes, faBackward } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-page-top-bar',
  templateUrl: './page-top-bar.component.html',
  styleUrls: ['./page-top-bar.component.scss']
})
export class PageTopBarComponent {
  @Input() heading: string = '';
  @Input() hasCloseButton = false;
  @Input() hasBackButton = false;
  @Output() leavePage = new EventEmitter();
  @Output() closePage = new EventEmitter();
  faTimes = faTimes;
  faBackward = faBackward;

  constructor() {
  }

}
