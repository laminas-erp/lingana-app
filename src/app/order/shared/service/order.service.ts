import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {OrderSearch} from "../model/order-search.model";
import {catchError, map, Observable} from "rxjs";
import {Orders} from "../model/orders.model";
import {BaseApiResponse} from "../../../shared/model/base-api-response";
import {CommonAppService} from "../../../shared/service/common-app.service";
import {OrderItem} from "../model/order-item.model";
import {MessageService} from "../../../shared/service/message.service";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    public http: HttpClient,
    private commonAppService: CommonAppService,
    private messageService: MessageService,
  ) {
  }

  searchOrders(orderSearch: OrderSearch): Observable<Orders> {
    return this.http.get<BaseApiResponse>('/lerp-order-rest-order' + this.commonAppService.computeQueryParamsFromSearchObject(orderSearch)).pipe(map(
      response => {
        const orders = new Orders();
        orders.orders = response.arr;
        orders.countOrders = response.count;
        return orders;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getOrderByUuid(orderUuid: string): Observable<object> {
    return this.http.get<BaseApiResponse>('/lerp-order-rest-order/' + orderUuid).pipe(map(
      response => response.obj
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getOrderSummaryByUuid(orderUuid: string): Observable<BaseApiResponse> {
    return this.http.get<BaseApiResponse>('/lerp-order-ajax-order-summary/' + orderUuid);
  }

  getOrderItem(orderItemUuid: string): Observable<OrderItem> {
    return this.http.get<BaseApiResponse>('/lerp-order-rest-order-item/' + orderItemUuid).pipe(map(
      response => {
        if (response.success) {
          return response.obj as OrderItem;
        }
        this.messageService.showMessages('error', '', response.messages);
        return new OrderItem();
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getOrderItemsForOrder(orderUuid: string): Observable<OrderItem[]> {
    return this.http.get<BaseApiResponse>('/lerp-order-rest-order-item?order_uuid=' + orderUuid).pipe(map(
      response => response.arr
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }
}
