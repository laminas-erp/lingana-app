import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from "rxjs";
import {Order} from "../model/order.model";
import {OrderSearch} from "../model/order-search.model";
import {Autocomplete} from "../../../shared/model/autocomplete.model";
import {OrderService} from "../service/order.service";
import {AppSharedService} from "../../../shared/service/app-shared.service";
import {LocationService} from "../../../location/shared/service/location.service";
import {CustomerService} from "../../../customer/shared/service/customer.service";
import {Orders} from "../model/orders.model";

@Component({
  selector: 'app-order-search',
  templateUrl: './order-search.component.html',
  styleUrls: ['./order-search.component.scss']
})
export class OrderSearchComponent implements OnInit, OnDestroy {
  subscriptions = new Set<Subscription>();
  @Input() headingText = '';
  @Input() withCloseButton = false;
  @Output() orderClicked = new EventEmitter<string>();
  @Output() closed = new EventEmitter<number>();
  orders: Order[] = [];
  orderSearch = new OrderSearch();
  costCentres: Map<string, string> = new Map<string, string>();
  locationPlaces: Map<string, string> = new Map<string, string>();
  customerNos: Autocomplete[] = [];
  constructor(
    private orderService: OrderService,
    private appSharedService: AppSharedService,
    private locationService: LocationService,
    private customerService: CustomerService,
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.appSharedService.getCostCentreIdAssoc().subscribe((data: Map<string, string>) => {
      this.costCentres = data;
    }));
    this.subscriptions.add(this.locationService.getLocationPlaceAssoc().subscribe((data: Map<string, string>) => {
      this.locationPlaces = data;
    }));
    if (window.sessionStorage.getItem('useLastSearchOrder') === 'true') {
      this.orderSearch.exchangeObject(JSON.parse(window.localStorage.getItem('orderSearch') + ''));
      this.orderSearch.initOrder();
      window.sessionStorage.removeItem('useLastSearchOrder');
      this.loadOrders();
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  checkOrder(orderField: string) {
    this.orderSearch.computeOrder(orderField);
    this.loadOrders();
  }

  executeSearch() {
    this.orderSearch.offset = 0;
    this.loadOrders();
  }

  loadOrders(): void {
    window.localStorage.setItem('orderSearch', JSON.stringify(this.orderSearch));
    this.subscriptions.add(this.orderService.searchOrders(this.orderSearch).subscribe((orders: Orders) => {
      this.orders = orders.orders;
      this.orderSearch.count = orders.countOrders;
    }));
  }

  resetSearch() {
    this.orderSearch = new OrderSearch();
    this.loadOrders();
  }

  pageCurrentChanged(pageCurrent: number) {
    this.orderSearch.offset = pageCurrent * this.orderSearch.limit;
    this.loadOrders();
  }

  limitChanged(limit: number) {
    this.orderSearch.limit = limit;
    this.orderSearch.offset = 0;
    this.loadOrders();
  }

  itemClick(uuid: string) {
    this.orderClicked.emit(uuid);
  }

  closeClick() {
    this.closed.emit(1);
  }

  customerNoChanged() {
    if (this.orderSearch.customer_no.length < 2) {
      return;
    }
    this.subscriptions.add(this.customerService.autocompleteCustomerNo(this.orderSearch.customer_no).subscribe((customerNos: Autocomplete[]) => {
      this.customerNos = customerNos;
    }));
  }

  customerNoChange(customerNo: string): void {
    this.orderSearch.customer_no = customerNo;
    this.customerNos = [];
  }

  onlyFinishClicked(type: string) {
    if (type === 'finish') {
      this.orderSearch.only_finish = !this.orderSearch.only_finish;
      this.orderSearch.only_open = false;
    } else if (type === 'open') {
      this.orderSearch.only_open = !this.orderSearch.only_open;
      this.orderSearch.only_finish = false;
    }
  }
}
