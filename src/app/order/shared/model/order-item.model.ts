import {BaseClass} from "../../../shared/model/base-class.model";
import {OrderItemListItem} from "./order-item-list.model";

export class OrderItem extends BaseClass {
  order_item_uuid = '';
  order_uuid = '';
  order_item_id = 0;
  product_uuid = '';
  order_item_text_short = '';
  order_item_text_long = '';
  order_item_quantity = 0;
  order_item_quantity_copy = 0;
  quantityunit_uuid = '';
  order_item_price = 0;
  order_item_price_total = 0;
  order_item_time_create = '';
  order_item_time_update = '';
  order_item_time_export = '';
  cost_centre_id = 0;
  order_item_order_priority = 0;
  order_item_taxp = 0;
  order_item_price_total_end = 0;
  //
  order_no = 0;
  order_no_compl = '';
  order_label = '';
  customer_uuid = '';
  offer_uuid = '';
  cost_centre_id_order = 0;
  location_place_uuid = '';
  order_time_create = '';
  order_time_create_unix = 0;
  order_time_finish_real = '';
  order_time_request = '';
  user_uuid_create = '';
  user_uuid_update = '';
  product_dirty = false;
  product_structure = '';
  product_origin = '';
  product_type = '';
  product_text_part = '';
  product_text_short = '';
  product_text_long = '';
  product_text_license = '';
  product_text_license_remark = '';
  product_no_no = 0;
  cost_centre_label = '';
  quantityunit_name = '';
  quantityunit_label = '';
  quantityunit_resolution = 0;
  quantityunit_resolution_group = '';
  count_factoryorder = 0;
  sum_delivery_qntty = 0;
  sum_estimate_qntty = 0;
  sum_invoice_qntty = 0;
  sum_order_confirm_qntty = 0;
  sum_proforma_qntty = 0;
  sum_stockout = 0;
  is_stock_using = false;
  orderItemList: OrderItemListItem[] = [];

  copyQuantity() {
    this.order_item_quantity_copy = this.order_item_quantity;
  }
}
