import {BaseSearch} from "../../../shared/model/base-search.model";


export class OrderSearch extends BaseSearch {
  order_no_compl = '';
  order_label = '';
  customer_no = '';
  offer_no_compl = '';
  cost_centre_id = 0;
  location_place_uuid = '';
  order_time_create_from = '';
  order_time_create_to = '';
  order_time_finish_schedule_from = '';
  order_time_finish_schedule_to = '';
  order_time_finish_real_from = '';
  order_time_finish_real_to = '';
  user_uuid_create = '';

  // handle it as radio button
  only_open = true;
  only_finish = false;
}
