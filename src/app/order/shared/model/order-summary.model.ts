import {BaseClass} from "../../../shared/model/base-class.model";

export class OrderSummary extends BaseClass {
  total_sum = 0;
  total_sum_tax = 0;
  total_sum_end = 0;
}
