import {BaseClass} from "../../../shared/model/base-class.model";

export class OrderItemList {

  /**
   * flat list
   */
  private _orderItemListArr: OrderItemListItem[] = [];

  private _productUuidTop = '';

  /**
   * hierarchically
   */
  orderItemLists: OrderItemListItem[] = [];

  get orderItemListArr(): OrderItemListItem[] {
    return this._orderItemListArr;
  }

  set orderItemListArr(value: OrderItemListItem[]) {
    this._orderItemListArr = value;
  }

  get productUuidTop(): string {
    return this._productUuidTop;
  }

  set productUuidTop(value: string) {
    this._productUuidTop = value;
  }

  /**
   * Das Input Array muss genau die Reihenfolge sein, wie dargestellt wird.
   * Also, erstes Item - alle Childs, zweites Item - alle childs ...
   */
  computeHirarchy() {
    this._orderItemListArr.forEach((item, index, theArray) => {
      if (item.product_uuid_parent === this._productUuidTop) {
        const orderItemListItem = new OrderItemListItem(item);
        if (theArray[index - 1]?.order_item_list_uuid) {
          orderItemListItem.hasPrevious = true;
        }
        if (theArray[index + 1]?.order_item_list_uuid) {
          orderItemListItem.hasFollower = true;
        }
        this.orderItemLists.push(orderItemListItem);
        this.computeChilds(orderItemListItem);
      }
    });
  }

  computeChilds(p_orderItemListItem: OrderItemListItem) {
    this._orderItemListArr.forEach((item, index, theArray) => {
      if (item.product_uuid_parent === p_orderItemListItem.product_uuid) {
        const orderItemListItem = new OrderItemListItem(item);
        p_orderItemListItem.children.push(orderItemListItem);
        this.computeChilds(orderItemListItem);
      }
    });
  }

  computeProductUuidTop() {
    const uuids = Array.from(this._orderItemListArr, x => x.product_uuid);
    this._orderItemListArr.forEach((item, index, array) => {
      if (!uuids.includes(item.product_uuid_parent)) {
        this._productUuidTop = item.product_uuid_parent;
      }
    });
  }
}

export class OrderItemListItemBase extends BaseClass {
  order_item_list_uuid = '';
  order_item_uuid = '';
  product_uuid_parent = '';
  product_uuid = '';
  order_item_list_quantity = 0;
  order_item_list_order_priority = 0;
  // join
  product_structure = '';
  product_text_short = '';
  product_text_long = '';
  product_no_no = 0;
  quantityunit_label = '';
}

export class OrderItemListItem extends OrderItemListItemBase {
  product_briefing = '';
  quantityunit_uuid = '';
  order_uuid = '';
  cost_centre_id = 0;
  quantityunit_resolution = 0;
  quantityunit_resolution_group = '';
  count_factoryorder = 0;

  children: OrderItemListItem[] = [];
  hasPrevious = false;
  hasFollower = false;

  constructor(obj: object) {
    super();
    this.exchangeObject(obj);
  }

  hasChildren(): boolean {
    return this.children.length > 0;
  }
}
