import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrderRoutingModule} from './order-routing.module';
import {OrdersComponent} from './orders/orders.component';
import {OrderSearchComponent} from './shared/order-search/order-search.component';
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [
    OrdersComponent,
    OrderSearchComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    FormsModule,
    SharedModule,
  ],
  exports: [
    OrderSearchComponent
  ]
})
export class OrderModule {
}
