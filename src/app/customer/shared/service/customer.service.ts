import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {CustomerSearch} from '../model/customer-search.model';
import {Customer} from '../model/customer.model';
import {CustomerContact} from '../model/customer-contact.model';
import {Customers} from '../model/customers.model';
import {MessageService} from "../../../shared/service/message.service";
import {CommonAppService} from "../../../shared/service/common-app.service";
import {BaseApiResponse} from "../../../shared/model/base-api-response";
import {Autocomplete} from "../../../shared/model/autocomplete.model";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  public currentCustomerSource = new Subject<Customer>(); // ...to fire next()
  public currentCustomer = this.currentCustomerSource.asObservable(); // ...to listen for *Source.next()

  public currentCustomerUuidSource = new Subject<string>(); // ...to fire next()
  public currentCustomerUuid = this.currentCustomerUuidSource.asObservable(); // ...to listen for *Source.next()

  constructor(
    public http: HttpClient,
    private messageService: MessageService,
    private commonAppService: CommonAppService
  ) {
  }

  searchCustomerss(customerSearch: CustomerSearch): Observable<Customers> {
    return this.http.get<BaseApiResponse>('/lerp-customer-rest-customer' + this.commonAppService.computeQueryParamsFromSearchObject(customerSearch)).pipe(map(
      response => {
        const customers = new Customers();
        customers.customerCount = response.count;
        if (Array.isArray(response.arr)) {
          for (const cArr of response.arr) {
            const customer = new Customer();
            customer.exchangeObject(cArr);
            customers.customers.push(customer);
          }
        }
        return customers;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getCustomer(customerUuid: string): Observable<Customer> {
    return this.http.get<BaseApiResponse>('/lerp-customer-rest-customer/' + customerUuid).pipe(map(
      response => {
        return response.obj as Customer;
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  getCustomerContacts(customerUuid: string): Observable<CustomerContact[]> {
    return this.http.get<BaseApiResponse>('/lerp-contact-customer/' + customerUuid).pipe(map(
      response => {
        return response.arr as CustomerContact[];
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }

  updateCustomer(customerUuid: string, customer: Customer): Observable<BaseApiResponse> {
    return this.http.put<BaseApiResponse>('/lerp-customer-rest-customer/' + customerUuid, customer);
  }

  insertCustomer(customer: Customer): Observable<BaseApiResponse> {
    return this.http.post<BaseApiResponse>('/lerp-customer-rest-customer', customer.getAsFormData());
  }

  autocompleteCustomerNo(customerNo: string): Observable<Autocomplete[]> {
    return this.http.get<BaseApiResponse>('/lerp-customer-ajax-autocomplete-customer-no/' + customerNo).pipe(map(
      response => {
        return response.arr as Autocomplete[];
      }
    ), catchError(this.commonAppService.handleHttpErrorResponse));
  }
}
