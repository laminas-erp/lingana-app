import {BaseSearch} from "../../../shared/model/base-search.model";

export class CustomerSearch extends BaseSearch {
  customer_no = '';
  customer_label = '';
  customer_name = '';
  country_id = 0;
  industry_category_uuid = '';
}
