import {Contact} from "../../../shared/model/contact.model";

export class CustomerContact extends Contact {
  contact_customer_rel_uuid = '';
  customer_uuid = '';
}
