import {BaseClass} from "../../../shared/model/base-class.model";

export class Customer extends BaseClass {
  override htmlentitiesParts = [
    'customer_label',
    'customer_name',
    'customer_name2',
    'customer_street',
    'customer_city',
    'customer_note',
    'country_name',
    'skr_03_desc',
    'skr_03_code_deposit',
    'skr_03_desc_datev',
    'pay_term_text',
    'industry_category_label',
  ];
  customer_uuid = '';
  customer_no = 0;
  customer_label = '';
  customer_name = '';
  customer_name2 = '';
  customer_street = '';
  customer_zip = '';
  customer_city = '';
  country_id = 0;
  customer_tel = '';
  customer_fax = '';
  customer_www = '';
  customer_email = '';
  customer_lang_iso = '';
  customer_tax_id = '';
  skr_03_code = '';
  pay_term_code = 0;
  industry_category_uuid = '';
  customer_time_create = '';
  customer_time_update = '';
  customer_note = '';

  country_name = '';
  country_iso = '';
  skr_03_desc = '';
  skr_03_tax = 0;
  skr_03_code_deposit = '';
  skr_03_desc_datev = '';
  pay_term_text = '';
  industry_category_label = '';
}
