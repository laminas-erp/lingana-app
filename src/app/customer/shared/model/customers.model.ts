import {Customer} from './customer.model';

export class Customers {
  customers: Customer[] = [];
  customerCount: number = 0;
}
