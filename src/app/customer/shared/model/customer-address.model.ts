import {Address} from "../../../shared/model/address.model";

export class CustomerAddress extends Address {
  address_customer_rel_uuid = '';
  customer_uuid = '';
}
