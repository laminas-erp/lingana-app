import {Component, OnDestroy} from '@angular/core';
import {environment} from "../environments/environment";
import {MessageService} from "./shared/service/message.service";
import {CommonAppService} from "./shared/service/common-app.service";
import {Subscription} from "rxjs";
import {Event, NavigationStart, Router} from "@angular/router";
import {faIndustry} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {

  protected readonly faIndustry = faIndustry;
  subscriptions = new Set<Subscription>();
  title = environment.apiUrl;
  menuVisible = false;

  constructor(
    private commonAppService: CommonAppService,
    private messageService: MessageService,
    private router: Router,
  ) {
    this.commonAppService.resetApiBaseUrl();
    this.subscriptions.add(this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.closeMenu();
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  openMenu() {
    this.menuVisible = true;
  }

  public closeMenu() {
    this.menuVisible = false;
  }

  showAlert() {
    this.messageService.showMessage('info', 'Ein Test', 'Some text ...');
  }
}
