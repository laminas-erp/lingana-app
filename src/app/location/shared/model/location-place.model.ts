
import {LocationRoom} from './location-room.model';
import {LocationCase} from './location-case.model';
import {BaseClass} from "../../../shared/model/base-class.model";

export class LocationPlace extends BaseClass {
  location_place_uuid = '';
  location_place_id = 0;
  location_place_name = '';
  location_place_label = '';

  rooms: LocationRoom[] = [];

  computeMember(lcs: LocationCase[]) {
    for (let i = 0; i < lcs.length; i++) {
      if (
        (!lcs[i - 1] || lcs[i - 1].location_room_uuid !== lcs[i].location_room_uuid)
        && lcs[i].location_place_uuid === this.location_place_uuid
      ) {
        const room = new LocationRoom();
        room.exchangeObject(lcs[i]);
        room.computeMember(lcs);
        this.rooms.push(room);
      }
    }
  }
}
