import {BaseSearch} from "../../../shared/model/base-search.model";


export class LocationSearch extends BaseSearch {
  location_place_uuid = '';
  location_room_uuid = '';
  location_rack_uuid = '';
  location_row_uuid = '';
  location_case_uuid = '';
}
