import {BaseClass} from "../../../shared/model/base-class.model";

export class LocationCase extends BaseClass {
  'location_case_uuid' = '';
  'location_case_id' = 0;
  'location_case_name' = '';
  'location_case_label' = '';
  'location_row_uuid' = '';
  'location_row_id' = 0;
  'location_row_name' = '';
  'location_row_label' = '';
  'location_rack_uuid' = '';
  'location_rack_id' = 0;
  'location_rack_name' = '';
  'location_rack_label' = '';
  'location_room_uuid' = '';
  'location_room_id' = 0;
  'location_room_name' = '';
  'location_room_label' = '';
  'location_place_uuid' = '';
  'location_place_id' = 0;
  'location_place_name' = '';
  'location_place_label' = '';
  'location_label_compl' = '';
  'location_label_place' = '';
  'location_label_room' = '';
  'location_label_arcr' = '';
}
