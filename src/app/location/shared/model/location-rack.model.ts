
import {LocationCase} from './location-case.model';
import {LocationRow} from './location-row.model';
import {BaseClass} from "../../../shared/model/base-class.model";

export class LocationRack extends BaseClass {
  location_rack_uuid = '';
  location_rack_id = 0;
  location_rack_name = '';
  location_rack_label = '';
  location_room_uuid = '';
  location_room_id = 0;
  location_room_name = '';
  location_room_label = '';
  location_place_uuid = '';
  location_place_id = 0;
  location_place_name = '';
  location_place_label = '';

  rows: LocationRow[] = [];

  computeMember(lcs: LocationCase[]) {
    for (let i = 0; i < lcs.length; i++) {
      if (
        (!lcs[i - 1] || lcs[i - 1].location_row_uuid !== lcs[i].location_row_uuid)
        && lcs[i].location_rack_uuid === this.location_rack_uuid
      ) {
        const row = new LocationRow();
        row.exchangeObject(lcs[i]);
        row.computeMember(lcs);
        this.rows.push(row);
      }
    }
  }
}
