
import {LocationCase} from './location-case.model';
import {BaseClass} from "../../../shared/model/base-class.model";

export class LocationRow extends BaseClass {
  'location_row_uuid' = '';
  'location_row_id' = 0;
  'location_row_name' = '';
  'location_row_label' = '';
  'location_rack_uuid' = '';
  'location_rack_id' = 0;
  'location_rack_name' = '';
  'location_rack_label' = '';
  'location_room_uuid' = '';
  'location_room_id' = 0;
  'location_room_name' = '';
  'location_room_label' = '';
  'location_place_uuid' = '';
  'location_place_id' = 0;
  'location_place_name' = '';
  'location_place_label' = '';

  cases: LocationCase[] = [];

  computeMember(lcs: LocationCase[]) {
    for (let i = 0; i < lcs.length; i++) {
      if (
        (!lcs[i - 1] || lcs[i - 1].location_case_uuid !== lcs[i].location_case_uuid)
        && lcs[i].location_row_uuid === this.location_row_uuid
      ) {
        const lcase = new LocationCase();
        lcase.exchangeObject(lcs[i]);
        this.cases.push(lcase);
      }
    }
  }
}
