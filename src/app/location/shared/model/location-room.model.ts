
import {LocationRack} from './location-rack.model';
import {LocationCase} from './location-case.model';
import {BaseClass} from "../../../shared/model/base-class.model";

export class LocationRoom extends BaseClass {
  location_room_uuid = '';
  location_room_id = 0;
  location_room_name = '';
  location_room_label = '';
  location_place_uuid = 0;
  location_place_name = '';
  location_place_label = '';

  racks: LocationRack[] = [];

  computeMember(lcs: LocationCase[]) {
    for (let i = 0; i < lcs.length; i++) {
      if (
        (!lcs[i - 1] || lcs[i - 1].location_rack_uuid !== lcs[i].location_rack_uuid)
        && lcs[i].location_room_uuid === this.location_room_uuid
      ) {
        const rack = new LocationRack();
        rack.exchangeObject(lcs[i]);
        rack.computeMember(lcs);
        this.racks.push(rack);
      }
    }
  }
}
