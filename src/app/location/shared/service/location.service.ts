import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {LocationSearch} from '../model/location-search.model';
import {LocationPlace} from '../model/location-place.model';
import {LocationRack} from '../model/location-rack.model';
import {LocationCase} from '../model/location-case.model';
import {BaseApiResponse} from "../../../shared/model/base-api-response";
import {CommonAppService} from "../../../shared/service/common-app.service";
import {MessageService} from "../../../shared/service/message.service";

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private reloadLocationsSource = new Subject<boolean>(); // ...to fire next()
  public reloadLocations = this.reloadLocationsSource.asObservable(); // ...to listen for *.next()

  constructor(public http: HttpClient, private messageService: MessageService, private serviceTools: CommonAppService) {
  }

  reloadLocationsSubject(reload: boolean) {
    this.reloadLocationsSource.next(reload);
  }

  checkInsertDefaults(): Observable<boolean> {
    return this.http.get<BaseApiResponse>('/lerp-location-create-defaults').pipe(map(
      response => {
        return response.success;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  insertLocationCase(rowUuid: string, caseLabel: string = '', caseName: string = ''): Observable<boolean> {
    const form = new FormData();
    form.append('row_uuid', rowUuid);
    form.append('case_label', caseLabel);
    form.append('case_name', caseName);
    return this.http.post<BaseApiResponse>('/lerp-location-rest-case', form).pipe(map(
      response => {
        return response.success;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  updateLocationCase(lCase: LocationCase): Observable<boolean> {
    return this.http.put<BaseApiResponse>('/lerp-location-rest-case/' + lCase.location_case_uuid,
      {case_name: lCase.location_case_name, case_label: lCase.location_case_label}).pipe(map(
      response => {
        return response.success;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  insertLocationRow(rackUuid: string, rowName: string = ''): Observable<boolean> {
    const form = new FormData();
    form.append('rack_uuid', rackUuid + '');
    form.append('row_name', rowName);
    return this.http.post<BaseApiResponse>('/lerp-location-rest-row', form).pipe(map(
      response => {
        return response.success;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  insertLocationRack(roomUuid: string, rackName: string = ''): Observable<boolean> {
    const form = new FormData();
    form.append('room_uuid', roomUuid + '');
    form.append('rack_name', rackName);
    return this.http.post<BaseApiResponse>('/lerp-location-rest-rack', form).pipe(map(
      response => {
        return response.success;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationPlaces(): Observable<LocationPlace[]> {
    return this.http.get<BaseApiResponse>('/lerp-location-view-places').pipe(map(
      response => {
        return response.arr as LocationPlace[];
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationRacks(locationPlaceUuid: string): Observable<LocationRack[]> {
    return this.http.get<BaseApiResponse>('/lerp-location-view-racks?location_place_uuid=' + locationPlaceUuid).pipe(map(
      response => {
        return response.arr as LocationRack[];
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationCases(locationSearch: LocationSearch): Observable<LocationCase[]> {
    return this.http.get<BaseApiResponse>('/lerp-location-view-cases' + this.serviceTools.computeQueryParamsFromSearchObject(locationSearch)).pipe(map(
      response => {
        return response.arr as LocationCase[];
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationPlaceAssoc(): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-location-lists-locationplace-assoc').pipe(map(
      response => {
        return this.serviceTools.computeStringStringMap(response.obj);
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationRoomAssoc(locationPlaceUuid: string): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-location-lists-locationroom-assoc/' + locationPlaceUuid).pipe(map(
      response => {
        return this.serviceTools.computeStringStringMap(response.obj);
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationRackAssoc(locationRoomUuid: string): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-location-lists-locationrack-assoc/' + locationRoomUuid).pipe(map(
      response => {
        return this.serviceTools.computeStringStringMap(response.obj);
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationRowAssoc(locationRackUuid: string): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-location-lists-locationrow-assoc/' + locationRackUuid).pipe(map(
      response => {
        return this.serviceTools.computeStringStringMap(response.obj);
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  getLocationCaseAssoc(locationRowUuid: string): Observable<Map<string, string>> {
    return this.http.get<BaseApiResponse>('/lerp-location-lists-locationcase-assoc/' + locationRowUuid).pipe(map(
      response => {
        return this.serviceTools.computeStringStringMap(response.obj);
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }
}
