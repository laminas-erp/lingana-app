import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'de.linganaerp',
  appName: 'lingana-erp-app',
  webDir: 'dist/lingana_app',
  server: {
    androidScheme: 'https'
  },
  android: {
    buildOptions: {
      releaseType: 'APK',
      keystorePath: '/home/allapow/AndroidProjects/KeyStore/lingana.jks',
      keystorePassword: 'testText973',
      keystoreAlias: 'key_lingana',
      keystoreAliasPassword: 'testText973'
    }
  }
};

export default config;
