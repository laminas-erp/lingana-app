# build Angular

```shell
ng build
# with custom config
ng build --configuration development
ng build --configuration production
ng build -c development
```

**sync assets etc before build for capacitor**

```shell
# add Android after delete his folder
npx cap add android
# then you can sync
npx cap sync
```

**build for android**

```shell
npx capacitor build android
npx capacitor build android --configuration development
npx capacitor build android --configuration production
```

**open the project in android studio**

```shell
npx cap open android
npx cap run android
```
